<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Rakuten TV</title>
        <style>
        body{
            width: 1280px;
            height: 720px;
            overflow: hidden;
        }
        #notification {
            top:0;
            left: 0;
            position: absolute;
            overflow: hidden;
        }
        </style>
    </head>
    <body>
        <?php
            $LANGUAGE = $_GET["language"];
            $IMG_PATH = "images/".$LANGUAGE."-Device-notification-splash-pages.jpg";
         ?>
        <img id="notification" src="<?=$IMG_PATH?>">
    </body>
</html>
