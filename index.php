<?php
// Check if there are parameters after question marks and append it to the url.
$parameters = (strpos($_SERVER['REQUEST_URI'], "/?")) ? substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], "/?") + 1) : $_SERVER['REQUEST_URI'];
$appUrl = "https://hisense-app.wuaki.tv/".$parameters; // Url with parameters;
$landingPage = "notification.php?language=";

// Get the userAgent
$userAgent = $_SERVER['HTTP_USER_AGENT'] . "\n\n";

// Get the country and language
$countryCodeFromIp = geoip_country_code_by_name(getenv("REMOTE_ADDR"));
switch ($countryCodeFromIp) {
    case 'ES':
        $language="SP";
        break;
    case 'UK':
    case 'BE':
    case 'IE':
    case 'LU':
    default:
        $language="UK";
        break;
    case 'FR':
        $language="FR";
        break;
    case 'DE':
    case 'AT':
    case 'CH':
        $language="DE";
        break;
    case 'PT':
        $language="PT";
        break;
    case 'NL':
        $language="NL";
        break;
    case 'IT':
        $language="IT";
        break;
}

// Check if the software is before April "H02", "H03"

$check = strpos($userAgent, ".H02");
if (!$check){
    $check = strpos($userAgent, ".H03");
}

if (!$check) {
	header('Location: '.$appUrl); // Redirect to the APP
}else{
 	header('Location: '.$landingPage.$language); // Redirect to the Landing Page with the update msg
}
